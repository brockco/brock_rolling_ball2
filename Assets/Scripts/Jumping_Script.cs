﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Real_Jumping_Script : MonoBehaviour {

    public float speed = 10f;
    public Rigidbody rb;
    public bool PlayerIsOnTheGround = true;

    private void awake() {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void update() {
        float horizontal = Input.GetAxis("Horizontal") * Time.deltaTime * speed;
        float vertical =Input.GetAxis("Vertical") * Time.deltaTime * speed;

        transform.Translate(horizontal, 0, vertical);

        if(Input.GetButtonDown("Jump") && PlayerIsOnTheGround) {
            rb.AddForce(new Vector3(0, 5, 0), ForceMode.Impulse);
            PlayerIsOnTheGround = false;
        }
    }

    private void oncollisionenter(Collision collision) {
        if(collision.gameObject.name == "Ground") {
            PlayerIsOnTheGround = true;
        }
    }
}
