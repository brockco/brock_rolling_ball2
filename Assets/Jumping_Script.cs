﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jumping_Script : MonoBehaviour {

    public float speed = 10f;
    public Rigidbody rb;
    public bool PlayerIsOnTheGround = true;

    private void Start() {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update() {
        float horizontal = Input.GetAxis("Horizontal") * Time.deltaTime * speed;
        float vertical =Input.GetAxis("Vertical") * Time.deltaTime * speed;

        transform.Translate(horizontal, 0, vertical);

        if(Input.GetButtonDown("Jump") && PlayerIsOnTheGround) {
            rb.AddForce(new Vector3(0, 5, 0), ForceMode.Impulse);
        }
    }

    private void OnCollisionEnter(Collision collision) {
        if(collision.gameObject.name == "Ground") {
            PlayerIsOnTheGround = true;
        }
    }
}
